const {WebComponent} =MingRouter;

class UserCard extends WebComponent {
  static className = "UserCard";
  static tagName = "user-card";
  constructor(props) {
    super(props);
    this.props = props;
    console.log(this.constructor.name, "eee")
  }

  state = {
    age: 1,
    hobby:"111"
  }

  addCount() {
    console.log(44);
    let age = this.state.age + 1;
    this.setState({ age })
  }


  renderCss() {
    return (`
       div{
          color:blue
       }  
   `)
  }

  componentDidMount() {
    console.log("....componentDidMount")
  }

  render(props) {
    return `
       <button onclick="${this.selfName}.addCount()"> aa </button>
       <div> ${props.image}  </div>
       <div>age: ${this.state.age}  </div>
       <div>hobby: ${this.state.hobby}  </div>
   `;
  }
}


MingRouter.registWebComponent(UserCard)
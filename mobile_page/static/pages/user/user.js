
const {Page}=MingRouter;


export default await Page({
    name: "user",
    async mounted() {
        $(function(){
            let hash= location.hash;
            if(hash){
                let curTab=hash.slice(-1);
                if(Number.parseInt(curTab)){
                    pageObj.data.tab= hash.slice(-1);
                    mingRouter.render();
                }
            }
            $('.weui-navbar__item').on('click', function () {
                pageObj.data.tab=  $(this).attr("href").slice(-1);
                mingRouter.render();
            });
        });
    },
    data:{
        tab:1,
    },
    methods:{
        navbarC(tab){
            return pageObj.data.tab==tab?'weui-bar__item_on':''
        },
        cotentC(tab){
            return pageObj.data.tab==tab?'weui_tab_bd_item_active':''
        },
        addCount(){
            pageObj.data.count++;
            mingRouter.refresh();
        }
    }
})
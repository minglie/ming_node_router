const {Page}=MingRouter;
export default await Page({
    name: "tab",
    async mounted() {
        weui.tab('#tab',{
            defaultIndex: 0,
            onChange: function(index){
                console.log(index);
            }
        });


        weui.slider('#slider', {
            step: 10,
            defaultValue: 40,
            onChange: function(percent){
                console.log(percent);
            }
        });
    },
    data: {
         count:4,
         todos: [{text: '学习 JavaScript'}]
    },
    methods:{
        addCount(){
            pageObj.data.count++;
            mingRouter.render();
        }
    }
})
(function (window) {
    const M = {};
    /**
     * ming_mock
     */
    {
        var App = {
            _get: {},
            get(methodName, callback) {
                //在M.IO上注册一个方法
                M.IO.reg(methodName.replace("/", ""));
                App._get[methodName] = callback;
            },
            doget(methodName, params, callback) {
                req = {};
                res = {};
                req.params = params;
                res.send = function (d) {
                    callback(d);
                }.bind(this);
                App._get[methodName](req, res);
            }
        };
        //服务方法注册
        M.IO = {};
        M.IO.reg = function (methedName) {
            M.IO[methedName] = (param) => {
                return new Promise(
                    function (reslove) {
                        App.doget("/" + methedName, param, (d) => {
                            reslove(d);
                        })
                    }
                )
            }
        };
        M.get = function (url, param) {
            let u;
            App.doget(url, param, (d) => {
                u = d;
            });
            return u;
        };


        M.init = function () {
            //格式化日期
            Date.prototype.format = function (fmt) {
                var o = {
                    "M+": this.getMonth() + 1,                 //月份
                    "d+": this.getDate(),                    //日
                    "h+": this.getHours(),                   //小时
                    "m+": this.getMinutes(),                 //分
                    "s+": this.getSeconds(),                 //秒
                    "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                    "S": this.getMilliseconds()             //毫秒
                };
                if (/(y+)/.test(fmt)) {
                    fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
                }
                for (var k in o) {
                    if (new RegExp("(" + k + ")").test(fmt)) {
                        fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
                    }
                }
                return fmt;
            }
        };

        M.getUserInfo = function () {
            if (localStorage.getItem('userInfo') == null) {
                return {}
            }
            return JSON.parse(localStorage.getItem('userInfo'))
        }

        M.setUserInfo = async function (userInfoObj) {
            localStorage.setItem("userInfo", JSON.stringify(userInfoObj));
            let r = M.getUserInfo();
            return r;
        }

        M.getParameter = function (name) {
            var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
            var r = window.location.href.substr(window.location.href.indexOf('?')).substr(1).match(reg);
            if (r != null) return unescape(r[2]);
            return null;
        };

        const post = async (api, params = {}, headers) => {
            return new Promise((reslove, reject) => {
                fetch(api, {
                    method: 'POST',
                    mode: 'cors',
                    headers: headers || {
                        'Content-Type': 'application/json'
                    },
                    body: JSON.stringify(params)
                }).then(function (response) {
                    return response.json();
                }).then((res) => {
                    reslove(res)
                }).catch((err) => {
                    reject(err)
                });
            })
        }

        const get = async (api, params = {}, headers) => {
            let getData = "";
            if (params) {
                getData = window.M.urlStringify(params);
                if (api.indexOf("?") > 0) {
                    getData = "&" + getData;
                } else {
                    getData = "?" + getData;
                }
            }
            api = api + getData;
            return new Promise((reslove, reject) => {
                fetch(api, {
                    method: 'GET',
                    mode: 'cors',
                    headers: headers || {
                        'Content-Type': 'application/json'
                    }
                }).then(function (response) {
                    return response.json();
                }).then((res) => {
                    reslove(res)
                }).catch((err) => {
                    reject(err)
                });
            })
        };

        window.M = M;
        window.MIO = M.IO;
        //将ajax请求挂到全局对象M上
        M.init();
        window.M.request = {}
        window.M.request.get = get;
        window.M.request.post = post;
        window.app = App;
    }




})(window);




MingRouter.registWebComponent(
    class MingKuaidi extends MingRouter.WebComponent {
        static template=`https://ming-bucket-01.oss-cn-beijing.aliyuncs.com/static/webcomponent/kuaidi.html`;

        state = {
            age: 1,
            num:0
        }
        addAge() {
            let age = this.state.age + 1;
            this.setState({ age })
        }
        addNum() {
            this.state.num = this.state.num + 1;
            document.querySelector("ming-kuaidi").shadowRoot.querySelector("#numId").innerText="num:"+this.state.num ;
        }
    }

 )
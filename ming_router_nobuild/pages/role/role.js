const {Page}=MingRouter;


export default await Page({

    name: "role",
    async mounted() {
        let list= await MIO.todoList();
        Page.role.data.todos=list;
    },
    data: {
         count:4,
         todos: [{text: '学习 JavaScript'}]
    },
    methods:{
        addCount(){
            Page.role.data.count++;
            mingRouter.render();
        }
    }
})
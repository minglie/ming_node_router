import './config.js';  //全局配置
import './server.js';  //全局接口
import './import.js';  //全局组件

import router from './router.js'

const app = new Vue({
    router
}).$mount('#root')
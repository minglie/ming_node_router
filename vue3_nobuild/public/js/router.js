import User from '../../pages/user/user.js'
const  { createRouter,createWebHashHistory} =VueRouter;
const Bar = { template: '<div>bar</div>' }

const routes = [
    { path: '/user', component: User },
    { path: '/', component: Bar },
    { path: '/role', component: User }
]
const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

export default router;
import './config.js';  //全局配置
import './server.js';  //全局接口
import router from './router.js'
import  setupCustomComponents  from './customComponents.js'
const {createApp}=Vue;


const vueApp= createApp({})
vueApp.use(router);
vueApp.mount('#root');

setupCustomComponents(vueApp)
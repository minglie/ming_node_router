customElements.define("user-card",
    class App extends HTMLElement {
        constructor() {
            super();
            this.attachShadow({ mode: "open" });
        }
        connectedCallback() {
            this.shadowRoot.innerHTML = `
              <style>
                span {
                  background: steelblue;
                  padding: 5px;
                  color: white;
                }
                .user_img{
                    width: 20vw;
                }
              </style>
              <div >
                <span>CSS</span>
                  <img class="user_img" src="${this.getAttribute('image')}">
                  <button onClick="${this.getAttribute('onclick1')}(55)">++</button>
                  <h1 class="countId">3333</h1>
              </div>
        `;
        }
    });

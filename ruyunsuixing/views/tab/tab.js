importStyle("router-page-tab", "./views/tab/tab.css");

export default Page({
        name: "tab",
        data() {
            return {

            }
        },
        methods: {},
        computed: {},
       async mounted() {
        weui.tab('#tab',{
            defaultIndex: 0,
            onChange: function(index){
                console.log(index);
            }
        });

        weui.slider('#slider', {
            step: 10,
            defaultValue: 40,
            onChange: function(percent){
                console.log(percent);
            }
        });
    },
    }
)


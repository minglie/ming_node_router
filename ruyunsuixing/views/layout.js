

export default {
    template:`
            <div class="header">
               <AppHeader/>
            </div>
             <!--  页面的遮罩层-->
             
             
            <div v-if="$gloable_state.showCoverMask" class="coverMaskDiv" @click="clickGloableCoverMask"></div>
            <div v-if="$gloable_state.visableLeftMenu" class="left-nav">
               <div >
                <button class="layout-backhome-btn">回到首页</button>
                <div>
                      <MingMenu />
                </div>
              </div>
           </div>
           <div>
                <p v-if="invalidRoute">404 is  {{$route.path}}</p>
                <router-view v-else></router-view> 
           </div>`,
    methods:{
        clickGloableCoverMask(){
            vueApp.config.globalProperties.$gloable_state.showCoverMask=false;
            vueApp.config.globalProperties.$gloable_state.visableLeftMenu=false;
        }
    },
     data() {
        return {

        }
    },
    computed: {
        invalidRoute () {
            return !this.$route.matched || this.$route.matched.length === 0;
        }
    },
    activated(){

    },
    async mounted() {
        if(!this.$route.path.startsWith('/checklogin')){
            let userInfo=  M.getUserInfo();
            if(userInfo==null && M.isPc && !M.isWeiXin){
                let hashArr= location.hash.split("?")
                let query=hashArr.length==2?`?${hashArr[1]}`:"";
                const redictUrl="/ruyunsuixing/index.html#/login"+query;
                console.log("<======",redictUrl)
                window.location.href=redictUrl;
            }
        }

    }

}

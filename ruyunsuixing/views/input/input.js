const template=`<div 
 @click.stop="clickDiv"
 style="background-color: pink;height: 80vh">
 <div>
 AAAAAAAAA
</div>
 <ming-input  v-if="inputState.displayInput" class="font-red" onsend="MIO.myinputCallBack" ></ming-input>
</div>`


const { ref,reactive } =Vue;
const readersNumber = ref(0);
const inputState = reactive({ displayInput: true })

app.get("/myinputCallBack",(req,res)=>{

    inputState.displayInput=false;
})


export default  Page({
    template,
        name:"input",
        data() {
            return {

            }
        },
        methods:{
            clickDiv(e){
                console.log(e.target.tagName)
                if("MING-INPUT" !=e.target.tagName){
                    inputState.displayInput=!inputState.displayInput
                }

           }
        },
        computed:{

        },
        async mounted() {

        },
        setup(props, context) {
            return {
                readersNumber,
                inputState
            }
        }
    }
)


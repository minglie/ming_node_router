



export default  Page({
    name:"burndiskDetail",
    data() {
        return {
            dizhi:window.location.href,
            visableDownloadDialog: false,
            dependencies:[],
            sn:"",
            _id:"",
            diskCode:"",
            softId: "",
            remark:"",
            updatedAt:"",
            diskName:"",
            helpNum:0,
            maskStyle:"coverMaskDiv",
            //遮罩层
            showCoverMask:false,
            visableBudingList:false,
            visableBudingListIcon:"https://langjie.oss-cn-hangzhou.aliyuncs.com/space/root/project/ruyunsuixing/img/xiala01.png"
        }
    },
    methods:{
        changeDownType(downloadType){
            this.downloadType=downloadType;
        },
        async download(){
            this.softId="";
            this.maskStyle="coverMaskDiv";
            let userInfo= M.getUserInfo()
            if(userInfo==null){
                let diskCode=M.getParameter("diskCode");
                location.href="/member/ruyunsuixing?diskCode="+diskCode+"&redirect_uri=/burndiskDetail";
                return;
            }
            // return
            if(!M.isPc){
                this.visableDownloadDialog=true;
                this.showCoverMask=true;
            }else {
                let r=await  MIO.cloudDiskDownLoad({_id:this._id});
                if(r.code==200){
                    window.open(r.data)
                }else {
                    weui.toast(r.msg)
                }
            }
        },
        async itemDownLoad(item){
            this.softId=item.id;
            if(!M.isPc){
                this.maskStyle="coverMaskDiv";
                this.visableDownloadDialog=true;
                this.showCoverMask=true;
            }else {
                let r=await  MIO.cloudDiskDownLoad({sn:this.sn,softId:this.softId});
                if(r.code==200){
                    window.open(r.data)
                }
            }
        },
        downLoadCallBack(downloadType,memberMail){
            console.log(downloadType,memberMail)
            if(downloadType!=1){
                this.showCoverMask=1;
                this.visableDownloadDialog=false;
                //this.helpNum=1;
                 if(downloadType==2){
                     this.maskStyle="coverMaskDiv3";
                 }else {
                     this.maskStyle="coverMaskDiv2";
                 }
            }
            if(downloadType==1){
                MIO.cardInstallSendMail({sn:this.sn,softId:this.softId,to:memberMail})
            }
        },
        changeVisableBudingList(){
            this.visableBudingList=!this.visableBudingList;
            if(this.visableBudingList){
               this.visableBudingListIcon="https://langjie.oss-cn-hangzhou.aliyuncs.com/space/root/project/ruyunsuixing/img/xiala01.png";
            }else {
                this.visableBudingListIcon="https://langjie.oss-cn-hangzhou.aliyuncs.com/space/root/project/ruyunsuixing/img/shangla01.png";
            }
        }
    },
    computed:{

    },
    async mounted() {
        let diskCode = M.getParameter("diskCode");
        if (diskCode == null) {
            alert("不存在");
            return;
        }
        let userInfo=  M.getUserInfo();
        let unionid = M.getParameter("unionid");
        //alert(M.isWeiXin)
        if(M.isPc && userInfo==null && !M.isWeiXin){
            let hashArr= location.hash.split("?")
            let query=hashArr.length==2?`&${hashArr[1]}`:"";
            const redictUrl=`/ruyunsuixing/index.html#/login?redirect_uri=/burndiskDetail`+query;
            console.log("=========burndisk=======",redictUrl,"=========burndisk=======")
            window.location.href=redictUrl;
            return;
        }
        if (unionid && userInfo==null) {
            M.setUserInfo({unionid})
            await MIO.fetchMemberInfo({unionid});
            userInfo=  M.getUserInfo();
        }
        let res = await MIO.targetBurnDiskByDiskCode(diskCode);
        if (res.code != 200) {
            alert(res.msg);
            return;
        }
        let resData = res.data;
        this._id=resData._id;
        this.diskCode = diskCode;
        this.remark = resData.remark;
        this.diskName = resData.diskName;
        this.dependencies = resData.dependencies;
        this.updatedAt = resData.updatedAt.substr(0, 10);
        //this.sn=99
        //this.$data={sn:77}
        M.t = this
    }
}
)


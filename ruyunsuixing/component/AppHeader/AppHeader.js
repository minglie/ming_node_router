
if(M.isPc){
    importStyle("AppHeader","./component/AppHeader/AppHeaderPc.css");
}else {
    importStyle("AppHeader","./component/AppHeader/AppHeader.css");
}



const template=`
<header class="header-wrap">
        <div class="left-meau-btn" @click="changLeftMenuStatus"></div>
        <div class="avatar" @click="showBtn">
            <img :src="$gloable_state.userInfo.portrait">
            <div v-if="visableBtn">
                     <button @click="clickBtn">{{$gloable_state.userInfo.unionid?'退出':'登陆'}}</button>
            </div>
        </div>
    </header>
`;



export default {
    template,
    name: 'AppHeader',
    props: {
        msg: String
    },
    data() {
        return {
            isWeiXin:M.isWeiXin,
            isPc:M.isPc,
            visableBtn: false,
        }
    },
    methods:{
        changLeftMenuStatus(){
            if(!vueApp.config.globalProperties.$gloable_state.userInfo.unionid){
                return;
            }
            vueApp.config.globalProperties.$gloable_state.visableLeftMenu=!vueApp.config.globalProperties.$gloable_state.visableLeftMenu
            if(vueApp.config.globalProperties.$gloable_state.visableLeftMenu){
                vueApp.config.globalProperties.$gloable_state.showCoverMask=true;
            }else {
                vueApp.config.globalProperties.$gloable_state.showCoverMask=false;
            }
        },
        showBtn(){
            if(this.isPc && this.isWeiXin){
                this.visableBtn=!this.visableBtn;
            }
        },
        clickBtn(){
            setTimeout(  ()=>  {
                    this.visableBtn=false;
                     MIO.signOut();
                    // if(this.$gloable_state.userInfo.unionid){
                    //      MIO.signOut();
                    // }
              }
            )
        },

    },
    mounted() {
        window.aa=this;
    },

}
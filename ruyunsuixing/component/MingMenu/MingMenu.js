const template = `
         <div  v-for="menuItemData in menuItemDataList">
           <MingMenuItem :menuItemData="menuItemData" />
        </div>    
    `;

export default {
    template,
    name: 'MingMenu',
    data() {
        return {
            menuItemDataList: [
                {
                    "name": "我的下载",
                    "value": "/tab",
                    "children": [
                        {"name": "我的下载1", "value": "/?"},
                        {"name": "我的下载2", "value": "/tab"},
                        {"name": "我的下载3", "value": "/minginput"},
                    ],
                },
                {
                    "name": "我的说明书",
                    "value": "我的说明书",
                    "children": [
                        {"name": "说明书1", "value": "说明书1"},
                        {"name": "说明书2", "value": "说明书"},
                        {"name": "说明书3", "value": "说明书3"},
                    ],
                },

            ]
        }
    },
    methods: {},
    mounted() {

    },
}
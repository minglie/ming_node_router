const Common = {};

Common.getAuthToken= () => {
    let payload;
    try {
        payload = JSON.parse(localStorage.getItem('lj_token'));
        const { lj_token, endDate } = payload;
        if (Date.now() > endDate) {
            return null;
        } else {
            return lj_token;
        }
    } catch (error) {
        return null;
    }
};

export  default Common;
//M.removeUserInfo()
//localStorage.clear();

app.end((req,res)=>{

    console.log(req.url,res)

})

app.get("/targetBurnDiskBySn",async (req,res)=>{
    let r= await M.request.get(`/cardopen/targetBurnDiskBySn`,{
        sn:req.params
    });
    res.send(r);
    //res.send(M.mockData["targetBurnDiskBySn"])
})

app.get("/targetBurnDiskByDiskCode",async (req,res)=>{
    let r= await M.request.get(`/cardopen/targetBurnDiskByDiskCode`,{
        diskCode:req.params
    });
    res.send(r);
    //res.send(M.mockData["targetBurnDiskBySn"])
})




app.get("/cardInstallSendMail",async (req,res)=>{
    const {sn,to,softId,_id}=req.params;
    let unionid=vueApp.config.globalProperties.$gloable_state.userInfo.unionid;
    let r= await M.request.post(`/cardopen/sendMail`,{
        unionid:unionid,
        sn:sn,
        to:to,
        _id:_id,
        softId:softId
    });
    res.send(r)
})


app.get("/cloudDiskDownLoad",async (req,res)=>{
    const {sn,softId,_id}=req.params;
    let unionid=vueApp.config.globalProperties.$gloable_state.userInfo.unionid;
    let r= await M.request.post(`/cardopen/cloudDisk/download`,{
        sn:sn,
        softId:softId,
        _id:_id,
        unionid:unionid,
    });
    res.send(r)
})







import AppHeader from '../../component/AppHeader/AppHeader.js';
import DownLoadBottomDialog from '../../component/DownLoadBottomDialog/DownLoadBottomDialog.js';
import MingMenuItem from '../../component/MingMenu/MingMenuItem.js';
import MingMenu from '../../component/MingMenu/MingMenu.js';
import MingInput from "../../component/WebComponent/ming-input/ming-input.js";

export default function setupCustomComponents(app) {
     app.component(AppHeader.name, AppHeader);
     app.component(DownLoadBottomDialog.name, DownLoadBottomDialog);
     app.component(MingMenuItem.name, MingMenuItem);
     app.component(MingMenu.name, MingMenu)

     MingRouter.registWebComponent(MingInput);
}
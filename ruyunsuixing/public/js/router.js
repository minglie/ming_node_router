import Index from '../../views/index/index.js';
import Test from '../../views/test/test.js';
import Tab from '../../views/tab/tab.js';
import MingInput from '../../views/input/input.js';
import Login from '../../views/login/login.js';
import Checklogin from '../../views/checklogin/checklogin.js'
import BurndiskDetail from '../../views/burndiskDetail/burndiskDetail.js';

const  { createRouter,createWebHashHistory} =VueRouter;
const Bar = { template: '<div>bar</div>' }

const routes = [

    { path: '/', component: Index },
    { path: '/test', component: Test },
    { path: '/tab', component: Tab },
    { path: '/login', component: Login },
    { path: '/minginput', component: MingInput },
    { path: '/checklogin', component: Checklogin },
    { path: '/burndiskDetail', component: BurndiskDetail },

]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

export default router;
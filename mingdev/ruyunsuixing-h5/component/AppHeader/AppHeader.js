
if(M.isPc){
    importStyle("AppHeader","./component/AppHeader/AppHeaderPc.css");
}else {
    importStyle("AppHeader","./component/AppHeader/AppHeader.css");
}



const template=`
<header class="header-wrap">
        <div class="left-meau-btn"></div>
        <div class="avatar">
            <img :src="$gloable_state.userInfo.avatar">
        </div>
    </header>
`;



export default {
    template,
    name: 'AppHeader',
    props: {
        msg: String
    },
    data() {
        return {
            count: 0
        }
    }
}
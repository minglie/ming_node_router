export default  Page({
    name:"index",
    data() {
        return {
            dizhi:window.location.href,
            visableDownloadDialog: false,
            dependencies:[],
            sn:"",
            remark:"",
            updatedAt:"",
            diskName:"",
            helpNum:0,
            //遮罩层
            showCoverMask:false
        }
    },
    methods:{
        changeDownType(downloadType){
            this.downloadType=downloadType;
        },
        async download(){

        },
        downLoadCallBack(downloadType,memberMail){

        }
    },
    computed:{

    },
    async mounted() {
       // alert(33)
        wx.config({
            debug: false,
            appId: 'wx8f62b63ef7711e77',
            timestamp: 1435301651,
            nonceStr: 'HjLNRxOAmchWrRB',
            signature: '1D69DB6F464ADC26D93118A2EC8A85E624A3E692',
            jsApiList: [
                'checkJsApi',
                'onMenuShareTimeline',
                'onMenuShareAppMessage',
                'onMenuShareQQ',
                'onMenuShareWeibo',
                'onMenuShareQZone',
                'hideMenuItems',
                'showMenuItems',
                'hideAllNonBaseMenuItem',
                'showAllNonBaseMenuItem',
                'translateVoice',
                'startRecord',
                'stopRecord',
                'onVoiceRecordEnd',
                'playVoice',
                'onVoicePlayEnd',
                'pauseVoice',
                'stopVoice',
                'uploadVoice',
                'downloadVoice',
                'chooseImage',
                'previewImage',
                'uploadImage',
                'downloadImage',
                'getNetworkType',
                'openLocation',
                'getLocation',
                'hideOptionMenu',
                'showOptionMenu',
                'closeWindow',
                'scanQRCode',
                'chooseWXPay',
                'openProductSpecificView',
                'addCard',
                'chooseCard',
                'openCard'
            ]
        });

        wx.ready(function() {
           alert(33)
        })
    }
}
)


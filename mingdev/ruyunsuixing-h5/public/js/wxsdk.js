/**
 * 对jssdk常用方法进行包装
 * 本js依赖：
 *  1. jquery1.7.js 以上
 *  2. http://res.wx.qq.com/open/js/jweixin-1.0.0.js 微信官网jssdk
 * Created by zhuqiang on 2016/6/22 0022.
 */
var wxsdk = {
    url:{
        /** 获取jssdk授权参数的url地址 */
        jsSdkAuthData:function () {
            return "/wx/jssdk/getConfigData.action";
        }
    },
    auth:{
        /**
         * 页面加载完成的时候，调用本方法就能对当前页面进行jssdk授权
         *  1. 需要先配置后端服务器请求url地址：wxsdk.url.jsSdkAuthData()
         *  2. 后端服务可通过：wxsdk的 public static JsSdkAuthorityData get(String appId,String secret,String url) 取到配置参数
         *  3. 后端返回的参数详情 可以参考 本对象中的 jsSdkAuth 方法说明
         * @param appid 公众号appid
         */
        currentPageAutoJsSdkAuth:function (appid) {
            $.ajax({
                url: wxsdk.url.jsSdkAuthData(),
                type: "POST",
                dataType: "jsonp",
                data: {
                    "appId": appid,
                    "url": window.location.href
                },
                success: function (data) {
                    wxsdk.auth.currentPageAutoJsSdkAuth(data);
                }
            });
        },
        /**
         * 封装了 对 jssdk 授权时的代码
         * @param data : 授权参数；可通过后端服务获取到
         *   @NotNull   appId:
         *   @NotNull   nonceStr:
         *   @NotNull   signature:
         *   @NotNull   timestamp:
         * @param success 授权通过回调函数
         * @param fail 授权失败回调函数
         * @param isDebug 是否开启debug模式（jssdk原生的debug参数）
         */
        jsSdkAuth:function(data,success,fail,isDebug){
            /*
             * 注意：
             * 1. 所有的JS接口只能在公众号绑定的域名下调用，公众号开发者需要先登录微信公众平台进入“公众号设置”的“功能设置”里填写“JS接口安全域名”。
             * 2. 如果发现在 Android 不能分享自定义内容，请到官网下载最新的包覆盖安装，Android 自定义分享接口需升级至 6.0.2.58 版本及以上。
             * 3. 常见问题及完整 JS-SDK 文档地址：http://mp.weixin.qq.com/wiki/7/aaa137b55fb2e0456bf8dd9148dd613f.html
             *
             * 开发中遇到问题详见文档“附录5-常见错误及解决办法”解决，如仍未能解决可通过以下渠道反馈：
             * 邮箱地址：weixin-open@qq.com
             * 邮件主题：【微信JS-SDK反馈】具体问题
             * 邮件内容说明：用简明的语言描述问题所在，并交代清楚遇到该问题的场景，可附上截屏图片，微信团队会尽快处理你的反馈。
             */
            wx.config({
                debug: isDebug,
                appId: data.appId,
                nonceStr: data.nonceStr,
                signature: data.signature,
                timestamp: data.timestamp,
                jsApiList: [
                    'checkJsApi',
                    'onMenuShareTimeline',
                    'onMenuShareAppMessage',
                    'onMenuShareQQ',
                    'onMenuShareWeibo',
                    'hideMenuItems',
                    'showMenuItems',
                    'hideAllNonBaseMenuItem',
                    'showAllNonBaseMenuItem',
                    'translateVoice',
                    'startRecord',
                    'stopRecord',
                    'onRecordEnd',
                    'playVoice',
                    'pauseVoice',
                    'stopVoice',
                    'uploadVoice',
                    'downloadVoice',
                    'chooseImage',
                    'previewImage',
                    'uploadImage',
                    'downloadImage',
                    'getNetworkType',
                    'openLocation',
                    'getLocation',
                    'hideOptionMenu',
                    'showOptionMenu',
                    'closeWindow',
                    'scanQRCode',
                    'chooseWXPay',
                    'openProductSpecificView',
                    'addCard',
                    'chooseCard',
                    'openCard'
                ]
            });
            // config信息验证后会执行ready方法，所有接口调用都必须在config接口获得结果之后，config是一个客户端的异步操作，所以如果需要在页面加载时就调用相关接口，则须把相关接口放在ready函数中调用来确保正确执行。对于用户触发时才调用的接口，则可以直接调用，不需要放在ready函数中。
            wx.ready(function () {
                if(success){success()};
                //alert("debug:jsSDK授权成功，该方法，实际测试来看，无论是否成功 都会在 调用 config之后调用该方法，不知道是什么原因");
            });
            // config信息验证失败会执行error函数，如签名过期导致验证失败，具体错误信息可以打开config的debug模式查看，也可以在返回的res参数中查看，对于SPA可以在这里更新签名。
            wx.error(function (res) {
                if(fail){fail(res)};
                /* alert(apiName + JSON.stringify(res));*/
            });
        }
    },
    /**
     * 分享功能:
     * wx.ready(function () {
     *      请把该函数放在这里面调用
     *  });
     *  由于是js：参数用不到的请使用null占位
     */
    share:{
        /**分享类型*/
        type:{
            music:"music",
            video:"video",
            link:"link"
        },
        /** 默认配置 */
        defaultOptions:{
            /** 分享到朋友圈 可选置参数*/
            onMenuShareTimeline:{
                /** trigger(res) 用户点击菜单中的 “分享到朋友圈” 按钮锁触发的事件回调函数
                 * 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                 * */
                trigger:null,
                /** success(res) 分享成功后触发的事件回调函数 */
                success:null,
                /** cancel(res) 用户点击取消分享触发的事件回调函数 */
                cancel:null,
                /** fail(res) 分享失败回调函数 */
                fail:null
            },
            /** 分享给朋友 可选置参数*/
            onMenuShareAppMessage:{
                /** trigger(res) 用户点击菜单中的 “分享到朋友圈” 按钮锁触发的事件回调函数
                 * 不要尝试在trigger中使用ajax异步请求修改本次分享的内容，因为客户端分享操作是一个同步操作，这时候使用ajax的回包会还没有返回
                 * */
                trigger:null,
                /** success(res) 分享成功后触发的事件回调函数 */
                success:null,
                /** cancel(res) 用户点击取消分享触发的事件回调函数 */
                cancel:null,
                /** fail(res) 分享失败回调函数 */
                fail:null,
                /** 分享类型,@see share.type，默认link */
                type:"link",
                /** 如果type是music或video，则要提供数据链接，默认为空 */
                dataUrl:null
            }

        },
        /**
         * 分享到朋友圈
         * @param @notNull title 标题
         * @param @notNull link 分享之后，点击跳转到的链接
         * @param @notNull imgUrl 分享出去的小图标
         * @param options 可选参数 ：@see share.defaultOptions.onMenuShareTimeline
         */
        onMenuShareTimeline:function(title,link,imgUrl,options) {
            var defOnMenuShareTimeline = wxsdk.share.defaultOptions.onMenuShareTimeline;
            $.extend( defOnMenuShareTimeline,options);
            wx.onMenuShareTimeline({
                title: title,
                link: link,
                imgUrl: imgUrl,
                trigger: function (res) {
                    var trigger = defOnMenuShareTimeline.trigger;
                    if(trigger){trigger(res)};
                },
                success: function (res) {
                    var success = defOnMenuShareTimeline.success;
                    if(success){success(res)};
                },
                cancel: function (res) {
                    var cancel = defOnMenuShareTimeline.cancel;
                    if(cancel){cancel(res)};
                },
                fail: function (res) {
                    var fail = defOnMenuShareTimeline.fail;
                    if(fail){fail(res)};
                }
            });
        },
        /**
         * 分享给朋友，
         * @param @notNull title 标题
         * @param @notNull desc 分享描述
         * @param @notNull link 分享之后，点击跳转到的链接
         * @param @notNull imgUrl 分享出去的小图标
         * @param options 可选参数 ：@see share.defaultOptions.onMenuShareAppMessage
         */
        onMenuShareAppMessage:function (title,desc,link,imgUrl,options) {
            var defOnMenuShareAppMessage = wxsdk.share.defaultOptions.onMenuShareAppMessage;
            $.extend(defOnMenuShareAppMessage,options);
            wx.onMenuShareAppMessage({
                title: title,
                desc: desc,
                link: link,
                imgUrl: imgUrl,
                type: defOnMenuShareAppMessage.type,
                dataUrl: defOnMenuShareAppMessage.dataUrl,
                trigger: function (res) {
                    var trigger = defOnMenuShareAppMessage.trigger;
                    if(trigger){trigger(res)};
                },
                success: function (res) {
                    var success = defOnMenuShareAppMessage.success;
                    if(success){success(res)};
                },
                cancel: function (res) {
                    var cancel = defOnMenuShareAppMessage.cancel;
                    if(cancel){cancel(res)};
                },
                fail: function (res) {
                    var fail = defOnMenuShareAppMessage.fail;
                    if(fail){fail(res)};
                }
            });
        }
    }
}
(function (window) {
    const M = {};

    /**
     * 加载html文件 start
     */
    M._loadHtmlCache={};
    M._loadCssCache={};
    M.loadHtml= async function (htmlUrl){
        if(M._loadHtmlCache[htmlUrl]){
            return M._loadHtmlCache[htmlUrl];
        }
        return new Promise((resolve,reject)=>{
            fetch(htmlUrl).then(d=>d.text()).then(d=>{
                M._loadHtmlCache[htmlUrl]=d;
                resolve(d)
            })
        })
    }
    M.loadCss= async function (cssUrl){
        let cssContent=""
        if(M._loadCssCache[cssUrl]){
            cssContent= M._loadCssCache[cssUrl];
        }else {
            cssContent= await new Promise((resolve,reject)=>{
                fetch(cssUrl).then(d=>d.text()).then(d=>{
                    M._loadCssCache[cssUrl]=d;
                    resolve(d)
                })
            })
        }
        document.querySelector("#pageCss").innerHTML=cssContent;
    }

    M.setPageCss=function (cssContent){
        document.querySelector("#pageCss").innerHTML=cssContent;
    }

    M.html =function (htmlUrl){
        let r=  M._loadHtmlCache[htmlUrl]||"<h1>wait...</h1>";
        return r;
    }
    /**
     * 加载html文件 end
     */

    window.importStyle=(id,url)=>{
        let styleCss= document.querySelector("#"+id);
        if(styleCss==null){
            var head = document.getElementsByTagName('HEAD').item(0);
            var style  = document.createElement("link");
            style.id=id;
            style.rel  = "stylesheet";
            style.href = url;
            head.appendChild(style);
        }
    }


    window.Page=async (pageObj)=>{
        let pageName= pageObj.name;
        let htmlUrl=`./views/${pageName}/${pageName}.html`;
        let cssUrl=`./views/${pageName}/${pageName}.css`;
        if(!pageObj.template){
            await M.loadHtml(htmlUrl)
        }
        return {
            ...pageObj,
            template: pageObj.template || M.html(htmlUrl),
            beforeCreate: pageObj.beforeCreate|| (async ()=>await M.loadCss(cssUrl))
        }
    }

    M.isPc=(()=>{
        var sUserAgent = navigator.userAgent.toLowerCase();
        if (/ipad|iphone|midp|rv:1.2.3.4|ucweb|android|windows ce|windows mobile/.test(sUserAgent)) {
           return false;
        }
        return true;
    })()


    var App = {
        _get: {},
        get (methodName, callback) {
            //在M.IO上注册一个方法
            M.IO.reg(methodName.replace("/", ""));
            App._get[methodName] = callback;
        },
        doget(methodName,params,callback) {
            req = {};
            res = {};
            req.params = params;
            res.send = function (d) {
                callback(d);
            }.bind(this);
            App._get[methodName](req, res);
        }
    };
    //服务方法注册
    M.IO = {};
    M.IO.reg = function (methedName) {
        M.IO[methedName] = (param) => {
            return new Promise(
                function (reslove) {
                    App.doget("/" + methedName,param,(d)=>{
                        reslove(d);
                    })
                }
            )
        }
    };
    M.get = function (url, param) {
        let u;
        App.doget(url,param,(d)=>{
            u = d;
        });
        return u;
    };



    M.init = function () {
        //格式化日期
        Date.prototype.format = function (fmt) {
            var o = {
                "M+": this.getMonth() + 1,                 //月份
                "d+": this.getDate(),                    //日
                "h+": this.getHours(),                   //小时
                "m+": this.getMinutes(),                 //分
                "s+": this.getSeconds(),                 //秒
                "q+": Math.floor((this.getMonth() + 3) / 3), //季度
                "S": this.getMilliseconds()             //毫秒
            };
            if (/(y+)/.test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (this.getFullYear() + "").substr(4 - RegExp.$1.length));
            }
            for (var k in o) {
                if (new RegExp("(" + k + ")").test(fmt)) {
                    fmt = fmt.replace(RegExp.$1, (RegExp.$1.length == 1) ? (o[k]) : (("00" + o[k]).substr(("" + o[k]).length)));
                }
            }
            return fmt;
        }
    };


    M.removeUserInfo=function (){
        localStorage.removeItem("userInfo");
    }
    M.getUserInfo=function (){
        if(localStorage.getItem('userInfo')==null){
            return null
        }
        return JSON.parse(localStorage.getItem('userInfo'))
    }

    M.setUserInfo=async function (userInfoObj){
         localStorage.setItem("userInfo", JSON.stringify(userInfoObj));
         let r=  M.getUserInfo();
         return r;
    }

    M.getParameter = function (name) {
        var reg = new RegExp("(^|&)" + name + "=([^&]*)(&|$)", "i");
        var r = window.location.href.substr(window.location.href.indexOf('?')).substr(1).match(reg);
        if (r != null) return unescape(r[2]);
        return null;
    };

    M.urlStringify = function (obj) {
        if (obj !== null && typeof obj === 'object') {
            var keys = Object.keys(obj);
            var len = keys.length;
            var flast = len - 1;
            var fields = '';
            for (var i = 0; i < len; ++i) {
                var k = keys[i];
                var v = obj[k];
                var ks = k + "=";
                fields += ks + v;
                if (i < flast) {
                    fields += "&";
                }
            }
            return fields;
        }
        return '';
    };

    const translateApi=(api)=>{
        let url=M.config.baseUrl(api) ;
        if(!api.startsWith("http")){
            api=url
        }
        return api;
    }

    const post  = async (api, params = {},headers) => {
        api=translateApi(api)
        return new Promise((reslove, reject) => {
            fetch(api, {
                method: 'POST',
                mode: 'cors',
                headers: headers||{
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(params)
            }).then(function (response) {
                return response.json();
            }).then((res) => {
                reslove(res)
            }).catch((err) => {
                reject(err)
            });
        })
    }

    const get = async (api, params = {},headers) => {
        api=translateApi(api)
        let getData = "";
        if (params) {
            getData = window.M.urlStringify(params);
            if (api.indexOf("?") > 0) {
                getData = "&" + getData;
            } else {
                getData = "?" + getData;
            }
        }
        api = api + getData;
        return new Promise((reslove, reject) => {
            fetch(api, {
                method: 'GET',
                mode: 'cors',
                headers: headers||{
                    'Content-Type': 'application/json'
                }
            }).then(function (response) {
                return response.json();
            }).then((res) => {
                reslove(res)
            }).catch((err) => {
                reject(err)
            });
        })
    };
    window.M = M;
    window.MIO = M.IO;
//将ajax请求挂到全局对象M上
    M.init();
    window.M.request={}
    window.M.request.get=get;
    window.M.request.post=post;
    window.app = App;
})(window);
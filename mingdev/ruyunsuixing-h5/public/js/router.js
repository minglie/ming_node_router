 import Index from '../../views/index/index.js'


const  { createRouter,createWebHashHistory} =VueRouter;
const Bar = { template: '<div>bar</div>' }

const routes = [
    { path: '/bar', component: Bar },
    { path: '/', component: Index }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

export default router;
//M.removeUserInfo()



app.get("/targetBurnDiskBySn",async (req,res)=>{
    let r= await M.request.get(`/cardopen/targetBurnDiskBySn`,{
        sn:req.params
    });
    res.send(r);
    //res.send(M.mockData["targetBurnDiskBySn"])
})


app.get("/memberInfoByUnionId",async (req,res)=>{
    let r= await M.request.get(`/tools/memberInfoByUnionId`,{
        unionid:req.params
    });
    if(r.code==200){
        console.log(r)
        M.setUserInfo(r.data);
        vueapp.config.globalProperties.$gloable_state.userInfo=r.data;
    }
    res.send(r.data)
})


app.get("/cardInstallSendMail",async (req,res)=>{
    const {sn,to}=req.params;
    let unionid=vueapp.config.globalProperties.$gloable_state.userInfo.unionid;
    let r= await M.request.post(`/cardopen/sendMail`,{
        unionid:unionid,
        sn:sn,
        to:to
    });
    res.send(r)
})


app.get("/cloudDiskDownLoad",async (req,res)=>{
    const sn=req.params;
    let unionid=vueapp.config.globalProperties.$gloable_state.userInfo.unionid;
    let r= await M.request.post(`/cardopen/cloudDisk/download/${sn}`,{
        unionid:unionid,
    });
    res.send(r)
})
if(M.isPc){
    importStyle("router-page-index","./views/index/indexPc.css");
}else {
    importStyle("router-page-index","./views/index/index.css");
}


export default  Page({
    name:"index",
    data() {
        return {
            dizhi:window.location.href,
            visableDownloadDialog: false,
            dependencies:[],
            sn:"",
            remark:"",
            updatedAt:"",
            diskName:"",
            helpNum:0,
            //遮罩层
            showCoverMask:false
        }
    },
    methods:{
        changeDownType(downloadType){
            this.downloadType=downloadType;
        },
        async download(){
            let userInfo= M.getUserInfo()
            if(userInfo==null){
                let sn=M.getParameter("sn");
                location.href="/member/ruyunsuixing?sn="+sn;
                return;
            }
            // return
            if(!M.config.isPc){
                this.visableDownloadDialog=true;
                this.showCoverMask=true;
            }else {
                let r=await  MIO.cloudDiskDownLoad(this.sn);
                if(r.code==200){
                    window.open(r.data)
                }

            }
        },
        downLoadCallBack(downloadType,memberMail){
            console.log(downloadType,memberMail)
            if(downloadType!=1){
                this.showCoverMask=1;
                this.visableDownloadDialog=false;
                this.helpNum=1;
            }
            if(downloadType==1){
                MIO.cardInstallSendMail({sn:this.sn,to:memberMail})
            }
        }
    },
    computed:{

    },
    async mounted() {

    }
}
)


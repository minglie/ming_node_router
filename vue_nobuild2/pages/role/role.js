
export default await Page({
    name:"role",
    async mounted() {
        let list= await MIO.todoList();
        this.todos=list;
    },
     data:()=>  {
        return{
            todos: [],
            data: [{
                label: '一级 1',
                children: [{
                    label: '二级 1-1',
                    children: [{
                        label: '三级 1-1-1'
                    }]
                }]
            }, {
                label: '一级 2',
                children: [{
                    label: '二级 2-1'
                }
                ]
            }],
            defaultProps: {
                children: 'children',
                label: 'label'
            }
        }
    },
     methods:{
         handleNodeClick(v){
             console.log(v.children)
         }
     }
})


 import Index from '../../views/index/index.js'
 import User from '../../views/user/user.js'



const  { createRouter,createWebHashHistory} =VueRouter;
const Bar = { template: '<div>bar</div>' }

const routes = [
    { path: '/user', component: User },
    { path: '/', component: Index }
]

const router = createRouter({
    history: createWebHashHistory(),
    routes: routes
})

export default router;
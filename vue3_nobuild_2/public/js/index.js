import './config.js';  //全局配置
import './server.js';  //全局接口
import router from './router.js'
import gloable_state from "./state.js";
import  setupCustomComponents  from './customComponents.js'
const {createApp}=Vue;


const vueApp= createApp({})
vueApp.use(router);




setupCustomComponents(vueApp);

vueApp.config.globalProperties.$BASE_IMG_URL =(url)=>{
    return "https://langjie.oss-cn-hangzhou.aliyuncs.com/space/root/project/ruyunsuixing/img"+url;
}

vueApp.config.globalProperties.$gloable_state =gloable_state;

setTimeout(()=>{
    vueApp.mount('#root');
},300)


window.vueapp=vueApp;
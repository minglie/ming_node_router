
import AppHeader from '../../component/AppHeader/AppHeader.js';
import DownLoadBottomDialog from '../../component/DownLoadBottomDialog/DownLoadBottomDialog.js';



export default function setupCustomComponents(app) {
     app.component(AppHeader.name, AppHeader);
     app.component(DownLoadBottomDialog.name, DownLoadBottomDialog)


}